import React, { useState, useEffect } from 'react';
import { NavLink } from 'react-router-dom';
import { BackgroundGradeContainer, PageContainer, StyledWrapper } from 'components/lib/ContainerStyles';
import { BackgroundLine } from 'components/lib/BackgroundLine';
import { Header } from 'components/lib/Header';
import { Loader } from 'components/lib/loader';
import { ArrowWhite, GoBackButtonResponsive } from 'components/lib/Buttons';
import { ListParagraphNews } from 'components/lib/Paragraphs';
import { PlaceHolder } from 'components/lib/PlaceHolder';
import { ListWrapper, HeaderContainer, ListItemCardNews, ListHeader, ListContainerNews, StyledH5, ListDetailsSpan, ListDetailsSectionNews, LoaderContainer, NewsImage } from './styles_calendar_news/ListStyles';

export const News = () => {
  const [list, setList] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    const fetchEventList = async () => {
      try {
        const url = process.env.REACT_APP_NEWS_LIST_URL
        if (!url) {
          throw new Error('Failed to fetch event list');
        }
        const response = await fetch(url);
        const data = await response.json();
        setList(data);
      } catch (error) {
        console.error(error);
      } finally {
        setLoading(false)
      }
    };

    fetchEventList();
  }, []);

  return (
    <>
      <Header isSmall />
      <PageContainer $news>
        <GoBackButtonResponsive />
        <StyledWrapper>
          <BackgroundGradeContainer $grade="#eec342, #eec342" $img={`${process.env.PUBLIC_URL}/assets/dance-line.png`} />
          <BackgroundLine isNews isCover backgroundimg={`${process.env.PUBLIC_URL}/assets/line03-01.svg`} />
        </StyledWrapper>
        <ListWrapper $right>
          <HeaderContainer $right>
            <ListHeader $news>AKTUELLT</ListHeader>
          </HeaderContainer>
          <ListContainerNews $dark>
            {!loading && list.length > 0 && list.map((listItem) => {
              return (
                <NavLink to={`/aktuellt/${listItem.newsid}`} key={listItem.newsid}>
                  <ListItemCardNews key={listItem.newsid} className="noted">
                    {listItem.picture && (
                      <NewsImage src={`https://www.tangonorte.com/img/www.tangonorte.com/page/${listItem.picture}`} alt="news" />
                    )}
                    <ListDetailsSectionNews>
                      <ListDetailsSpan $news>
                        <StyledH5>{listItem.title}</StyledH5>
                        <ListParagraphNews>
                          {listItem.body_clean}
                        </ListParagraphNews>
                      </ListDetailsSpan>
                      <ArrowWhite isSmall isWhite />
                    </ListDetailsSectionNews>
                  </ListItemCardNews>
                </NavLink>
              )
            })}
            {!loading && list.length === 0 && (
              <PlaceHolder />
            )}
            {loading
              && (
                <LoaderContainer>
                  <Loader />
                </LoaderContainer>
              )}
          </ListContainerNews>
        </ListWrapper>
      </PageContainer>
    </>
  );
}

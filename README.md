# Tango Norte frontida

## Installationsguide

1. Ladda ner node.js härifrån på din dator globalt: 
https://nodejs.org/en
Den rekommenderade versionen är den rekommenderade

2. Klona repon

```
https://gitlab.com/Vera-Sjunnesson/tango-norte.git

```

3. Navigera in i projektet och sen

```
cd frontend

```
där package.json filen ligger

4. Installera npm

```
npm install

```

5. För att se projektet i browsern

```
npm start
```

6. Om du stöter på problem, kontrollera att följande dependencies är korrekt installererade och att det inte uppstått några konflikter mellan dem (se package.json filen)

## Dependencies

- React
- React-router-dom
- Styled Components
- react-global-style
- React-responsive
- Swiper
- framer-motion
- React Lottie player
- lottie-react
- dotenv
- Eslint and related packages
- react-big-calendar
- react-hook-form (används ej)
- Material UI (används ej i nuläget)
- svgo (används ej nu)

## Technologies used in this repository:
![React](https://img.shields.io/badge/react-%2320232a.svg?style=for-the-badge&logo=react&logoColor=%2361DAFB)
![JavaScript](https://img.shields.io/badge/javascript-%23323330.svg?style=for-the-badge&logo=javascript&logoColor=%23F7DF1E)
![React Router](https://img.shields.io/badge/React_Router-CA4245?style=for-the-badge&logo=react-router&logoColor=white)
![Styled Components](https://img.shields.io/badge/styled--components-DB7093?style=for-the-badge&logo=styled-components&logoColor=white)

## View it live

https://tango-norte-utkast.netlify.app/

## Visuals

<img width="300" alt="Tango Norte Demo 1" src="/frontend/public/images/tango-norte-demo.png">
<img width="300" alt="Tango Norte Demo 2" src="/frontend/public/images/tango-norte-demo3.png">
<img width="300" alt="Tango Norte Demo 3" src="/frontend/public/images/tango-norte-demo2.png">
<img width="300" alt="Tango Norte Demo 4" src="/frontend/public/images/tango-norte-demo4.png">